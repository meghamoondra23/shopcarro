package com.example.meghamoondra.navigationbar;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.meghamoondra.navigationbar.model.IAccount;
import com.example.meghamoondra.navigationbar.model.MerchantActivity;
import com.example.meghamoondra.navigationbar.model.MerchantModel;
import com.example.meghamoondra.navigationbar.model.Product;
import com.example.meghamoondra.navigationbar.model.ProductAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.meghamoondra.navigationbar.Urls.authenticationUrl;
import static com.example.meghamoondra.navigationbar.Urls.productUrl;


public class HomeFragment extends Fragment implements ProductAdapter.IAdapterCommunicator {
    List<Product> productList;
    RecyclerView recyclerView;
    IAccount iAccount;
    ProductAdapter productadapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_home_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView myrv = (RecyclerView) view.findViewById(R.id.rv_products);

        Button bt_search =view.findViewById(R.id.button);
        final EditText query =view.findViewById(R.id.editText);
        //initializing the productlist
        productList = new ArrayList<>();
        iAccount = AppController.getInstance().getClientProduct(productUrl).create(IAccount.class);

//        bt_search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                Intent intent;
//                intent = new Intent(getActivity(),CardView.class);
//                intent.putExtra("Search",query.getText() + "");
//                startActivity(intent);
//
//            }
//
//
//        });
        getAllProducts();



        productadapter = new ProductAdapter(productList,this);
        myrv.setLayoutManager(new GridLayoutManager(getContext(),2));
        myrv.setAdapter(productadapter);

        //setting adapter to recyclerview
        myrv.setAdapter(productadapter);


    }

    private void getAllProducts() {
        Call<Product[]> call = iAccount.getAll();
        call.enqueue(new Callback<Product[]>() {
            @Override
            public void onResponse(Call<Product[]> call, Response<Product[]> response) {


                if (response.code() >= 200 && response.code() < 300) {
                    productList.addAll(Arrays.asList(response.body()));

                    productadapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onFailure(Call<Product[]> call, Throwable t) {
                Toast.makeText(getActivity(), "FAILURE", Toast.LENGTH_SHORT).show();

            }
        });

    }


    @Override
    public void deleteItem(int position) {

    }

    @Override
    public void navigateDetail(MerchantModel merchantModel,String imageUrl) {
        Intent intent=new Intent(getActivity(),MerchantActivity.class);
        intent.putExtra("Product", merchantModel);
        intent.putExtra("ImageUrl",imageUrl);
        Log.d("HomeImage", "navigateDetail: "+imageUrl);
        startActivity(intent);
    }
}



