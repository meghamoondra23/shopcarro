package com.example.meghamoondra.navigationbar.model;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.meghamoondra.navigationbar.AppController;
import com.example.meghamoondra.navigationbar.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.meghamoondra.navigationbar.Urls.productUrl;
import static com.example.meghamoondra.navigationbar.Urls.searchUrl;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private static IAccount iAccount;
    Product productDetails = new Product();

    private List<SearchProduct> mProduct;
    private IAdapterCommunicator iAdapterCommunicator;
    //IAccount iAccount;
    public SearchAdapter(List<SearchProduct> products, IAdapterCommunicator iAdapterCommunicator) {
        mProduct = products;
        this.iAdapterCommunicator = iAdapterCommunicator;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Input the layout id
        View view =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.list_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final SearchProduct product = mProduct.get(position);
        holder.productName.setText(product.getProductName());
        holder.productDesc.setText(product.getDescription());
        holder.productRating.setText("Merchants Selling: " + product.getMerchantId());
        holder.productPrice.setText(product.getPrice().toString());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("InsideCardView","this");
                getProductsDetails(product.getId());

//                MerchantModel merchantModel=new MerchantModel();
//                String merchantIds = product.getMerchantId();
//                String productId = product.getId();
//                List<StockId> stockIds = new ArrayList<>();
//                for(String merchantId : merchantIds) {
//                    stockIds.add(new StockId(merchantId, productId));
//                }
              //  Log.d(TAG, "onClick: " + stockIds);
                //merchantModel.setStockIds(stockIds);

                //iAdapterCommunicator.navigateDetail(merchantModel,product.getImgUrl());
            }
        });

            Glide.with(holder.imageView.getContext())
                    .load(product.getImgUrl())
                    .into(holder.imageView);
        }


        //Binding UI



    @Override
    public int getItemCount() {
        // Set the list count

        return mProduct.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Declare the views
        RelativeLayout cardView;
        TextView productName;
        TextView productDesc;
        TextView productRating;
        TextView productPrice;
        ImageView imageView;
        //Button btDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            cardView =itemView.findViewById(R.id.productCard);
            productName = itemView.findViewById(R.id.textViewTitle);
            productDesc = itemView.findViewById(R.id.textViewShortDesc);
            productRating = itemView.findViewById(R.id.textViewRating);
            productPrice = itemView.findViewById(R.id.textViewPrice);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            //btDelete = itemView.findViewById(R.id.bt_delete);

        }
    }

    public void getProductsDetails(String productId){
        iAccount = AppController.getInstance().getClientProduct(productUrl).create(IAccount.class);
        Call<Product> call = iAccount.get(productId);
        //Log.d("anything", newAccount.toString());

        call.enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                productDetails = response.body();
                Log.d("123",productDetails.toString());
                MerchantModel merchantModel=new MerchantModel();
                List<String> merchantIds = new ArrayList<>();
                merchantIds = productDetails.getMerchantId();
                String productId = productDetails.getId();
                List<StockId> stockIds = new ArrayList<>();
                for(String merchantId : merchantIds) {
                    stockIds.add(new StockId(merchantId, productId));
                }
                merchantModel.setStockIds(stockIds);
                iAdapterCommunicator.navigateDetail(merchantModel,productDetails.getImgUrl());
                //Toast.makeText(, "Show description page", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                Log.d("searchAdapter","On Failure from Search Adapter");
                //Toast.makeText(get, "OnFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public static interface IAdapterCommunicator {
        void deleteItem(int position);
        void navigateDetail(MerchantModel merchantModel, String imageUrl);
    }
}

