package com.example.meghamoondra.navigationbar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.meghamoondra.navigationbar.model.IAccount;
import com.example.meghamoondra.navigationbar.model.NewAccount;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.example.meghamoondra.navigationbar.Urls.authenticationUrl;

public class SignUpActivity extends AppCompatActivity {
    EditText email, firstName, lastName, phoneNumber, address, password;
    IAccount iAccount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        email=findViewById(R.id.et_email);
        firstName=findViewById(R.id.et_firstname);
        lastName=findViewById(R.id.et_lastname);
        phoneNumber=findViewById(R.id.et_number);
        address=findViewById(R.id.et_address);
        password=findViewById(R.id.et_password);

        AppController appController = AppController.getInstance();
        Retrofit retrofit = appController.getClientAuthentication(authenticationUrl);
        iAccount = retrofit.create(IAccount.class);


        findViewById(R.id.bt_signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isValidEmail(email.getText()) && isValidPassword(password.getText().toString()) && phoneNumber.length()==10) {

                    NewAccount newaccount = new NewAccount();

                    newaccount.setEmail(email.getText().toString());
                    newaccount.setFirstName(firstName.getText().toString());
                    newaccount.setLastName(lastName.getText().toString());
                    newaccount.setPassword(password.getText().toString());
                    newaccount.setPhoneNumber(phoneNumber.getText().toString());
                    newaccount.setAddress(address.getText().toString());

                    addAccount(newaccount);
                }
                else{
                    Toast.makeText(getApplicationContext(), "Enter valid EmailId or Password", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addAccount(NewAccount newAccount) {
        Call<Boolean> call = iAccount.add(newAccount);
        Log.d("anything", newAccount.toString());
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if(response.body()){
                    Toast.makeText(getApplicationContext(), "Login once Again", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                    startActivity(intent);

                }

                Toast.makeText(getApplicationContext(), "Failure", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "OnFailure", Toast.LENGTH_SHORT).show();
            }
        });

    }
    public boolean isValidEmail(CharSequence email) {
        if (email == null) {
            return false;
        }
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean isValidPassword(String password){

        if(password.length()<8 || password==null){
            return false;
        }
        return true;
    }

}
