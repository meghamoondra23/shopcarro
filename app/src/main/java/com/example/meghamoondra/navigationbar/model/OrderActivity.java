package com.example.meghamoondra.navigationbar.model;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.meghamoondra.navigationbar.AppController;
import com.example.meghamoondra.navigationbar.LoginActivity;
import com.example.meghamoondra.navigationbar.ProductDetails;
import com.example.meghamoondra.navigationbar.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.meghamoondra.navigationbar.Urls.addCartUrl;
import static com.example.meghamoondra.navigationbar.Urls.orderUrl;

public class OrderActivity extends AppCompatActivity implements OrderAdapter.IAdapterCommunicator {

    List<History> productList;
    //IAccount iAccount;
    OrderAdapter orderAdapter;
    //the recyclerview
    RecyclerView recyclerView;
    String email;
    String productId;
    String merchantId;
    private static final String ShopcarroPref = "ShopCarroPref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final SharedPreferences sharedPreferences = getSharedPreferences(ShopcarroPref, Context.MODE_PRIVATE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view);
        //getting the recyclerview from xml
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        if (sharedPreferences.contains("username")) {
            if (sharedPreferences.getString("username", null) != null) {
                email = sharedPreferences.getString("username", null);
              //  Log.d("qwerty",productId + " " +merchantId + " " + email);
                Toast.makeText(getApplicationContext(),"History",Toast.LENGTH_LONG).show();
                productList = new ArrayList<>();
                getHistory(email);
                //initializing the productlist
                orderAdapter = new OrderAdapter(productList,this);
                //setting adapter to recyclerview
                recyclerView.setAdapter(orderAdapter);
                Log.d("check", "onCreate: Megha");
            }
            else{
                Toast.makeText(getApplicationContext(),"Go to login",Toast.LENGTH_LONG).show();
                Intent i = new Intent(OrderActivity.this, LoginActivity.class);
                startActivity(i);
            }
        }
        else{
            Toast.makeText(getApplicationContext(),"Go to login",Toast.LENGTH_LONG).show();
            Intent i = new Intent(OrderActivity.this, LoginActivity.class);
            startActivity(i);
        }

    }

//    private void getAddCart(String email, String merchantId, String productId) {
//        IAccount iAccount = AppController.getInstance().getClientAddCart(addCartUrl).create(IAccount.class);
//        Call<Boolean> call = iAccount.getCartProducts(email,merchantId,productId);
//        call.enqueue(new Callback<Boolean>() {
//            @Override
//            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
//                if (response.code() >= 200 && response.code() < 300) {
//                    if(response.body()){
//                        Toast.makeText(getApplication(), "Product added to cart", Toast.LENGTH_SHORT).show();
//                    }
//                    else{
//                        Intent i = new Intent(OrderActivity.this, LoginActivity.class);
//                        startActivity(i);
//                    }
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Boolean> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "Cart Failure", Toast.LENGTH_SHORT).show();
//                //progressDialog.dismiss();
//            }
//        });
//
//    }

    private void getHistory(String email) {
        IAccount iAccount = AppController.getInstance().getClientOrder(orderUrl).create(IAccount.class);
        Call<List<History>> call = iAccount.getUserHistory(email);
        call.enqueue(new Callback<List<History>>() {
            @Override
            public void onResponse(Call<List<History >> call, Response<List<History> > response) {
                Log.d("345",response.body().toString());
                if (response.code() >= 200 && response.code() < 300) {
                    Log.d("345",response.body().toString());
                    //productList.clear();
                    productList.addAll(response.body());
                    Toast.makeText(getApplication(), "Past History", Toast.LENGTH_LONG).show();
                    orderAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<History > > call, Throwable t) {
                Toast.makeText(getApplicationContext(), "FAILURE", Toast.LENGTH_SHORT).show();

            }
        });
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void deleteItem(int position) {

    }

    @Override
    public void navigateDetail(String productId) {

    }
}

