package com.example.meghamoondra.navigationbar.model;

public class OrderDetails {

    private String id;
    private String merchantId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMerchantId() {
        return merchantId;
    }

    @Override
    public String toString() {
        return "OrderDetails{" +
                "id='" + id + '\'' +
                ", merchantId='" + merchantId + '\'' +
                '}';
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public OrderDetails(String productId, String merchantId) {
        this.id = productId;
        this.merchantId = merchantId;
    }
}
