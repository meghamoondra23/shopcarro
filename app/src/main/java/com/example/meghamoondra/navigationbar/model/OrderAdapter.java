package com.example.meghamoondra.navigationbar.model;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.meghamoondra.navigationbar.R;

import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder>{

    private List<History> mProduct;

    private OrderAdapter.IAdapterCommunicator iAdapterCommunicator;

    public OrderAdapter(List<History> products, IAdapterCommunicator iAdapterCommunicator) {
        mProduct = products;
        this.iAdapterCommunicator = iAdapterCommunicator;
    }

    //public OrderAdapter(List<OrderDetails> productList, OrderActivity merchantActivity) {
   // }


    @NonNull
    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Input the layout id
        View view =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.order_history, parent, false);
        return new OrderAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final History product = mProduct.get(position);
        Log.d("Check", "onBindViewHolder: " + product);

       // holder.productName.setText(product.getEmail());
       // OrderDetails orderdetail=(OrderDetails) product.getDetails();
        holder.id.setText("Product Id :" + product.getProductId());
        holder.hmerchantid.setText("Merchant Id :" + product.getMerchantId());
        holder.productName.setText("Product Name :" + product.getProductName());
        //holder.productPrice.setText(product.getNoOfItems() + "");
        //Log.d("imageHolder", ""+mimageUrl);
      //  holder.cardView.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View v) {
       //         iAdapterCommunicator.navigateDetail(product.getId().toString(),product.getMerchantId());
         //   }
       // });
        // if (mimageUrl == null || mimageUrl.isEmpty()) {
        //   holder.imageView.setBackground(ContextCompat.getDrawable(holder.imageView.getContext(),1));
        // } else {
//        Glide.with(holder.imageView.getContext())
//                .load(mimageUrl)
//                .into(holder.imageView);
        //  }

    }

    @Override
    public int getItemCount() {
        // Set the list count
        return mProduct.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Declare the views
     //   RelativeLayout cardView;
        TextView id;
        TextView hmerchantid;
        TextView productName;
        //TextView productRating;
       // TextView productPrice;
        //ImageView imageView;
        //Button btDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            //cardView =itemView.findViewById(R.id.productCard);
            id = itemView.findViewById(R.id.hid);
            hmerchantid = itemView.findViewById(R.id.hMid);
            productName = itemView.findViewById(R.id.hname);
          //  productRating = itemView.findViewById(R.id.textViewRating);
          //  productPrice = itemView.findViewById(R.id.textViewPrice);
            //imageView = (ImageView) itemView.findViewById(R.id.imageView);
            //btDelete = itemView.findViewById(R.id.bt_delete);

        }
    }

    public static interface IAdapterCommunicator {
        void deleteItem(int position);
        void navigateDetail(String productId);
    }
}



