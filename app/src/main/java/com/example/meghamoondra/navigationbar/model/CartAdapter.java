package com.example.meghamoondra.navigationbar.model;
//package com.example.meghamoondra.navigationbar.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.meghamoondra.navigationbar.R;

import java.util.ArrayList;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder>{
//    private static final String ShopcarroPref = "ShopCarroPref";
    private static final String TAG = "CartAdapter";
    //CartProduct product1;
//    SharedPreferences sharedPreferences;
    private List<CartDetails> mProduct;
//    private List<CartDetails> product1 = new ArrayList<>();

    private CartAdapter.IAdapterCommunicator iAdapterCommunicator;
    public CartAdapter(List<CartDetails> products, CartAdapter.IAdapterCommunicator iAdapterCommunicator) {
        mProduct = products;
        this.iAdapterCommunicator = iAdapterCommunicator;
    }



    @NonNull
    @Override
    public CartAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Input the layout id

        View view =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_listlayout, parent, false);
        return new CartAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
       // final CartProduct product = mProduct.get(position);
        //mProduct=product1.getDetails();

        final CartDetails productList= mProduct.get(position);
        Log.d(TAG, "data1: " + productList);

       // Log.d("Check", "onBindViewHolder: " + product);
        //CartDetails cartDetails=(CartDetails) product.getDetails();
        holder.productId.setText("Product Id : "+productList.getId()+"");
        holder.merchantId.setText("Merchant Id : "+productList.getMerchantId() + "");
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iAdapterCommunicator.deleteItem(holder.getAdapterPosition());
            }
        });
       // holder.productRating.setText(product.getRating() + "");
        //holder.productPrice.setText(product.getNoOfItems() + "");
        //Log.d("imageHolder", ""+mimageUrl);
//        holder.cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                iAdapterCommunicator.navigateDetail(product.getId().toString(),product.getMerchantId());
//            }
//        });
        // if (mimageUrl == null || mimageUrl.isEmpty()) {
        //   holder.imageView.setBackground(ContextCompat.getDrawable(holder.imageView.getContext(),1));
        // } else {
//        Glide.with(holder.imageView.getContext())
//                .load(mimageUrl)
//                .into(holder.imageView);
//        //  }

    }




    @Override
    public int getItemCount() {
        // Set the list count
        return mProduct.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Declare the views
        RelativeLayout cardView;
        TextView productId;
        TextView merchantId;
        Button delete;
        TextView productRating;
        TextView productPrice;
        ImageView imageView;
        //Button btDelete;

        public ViewHolder(View itemView) {
            super(itemView);
          //  cardView =itemView.findViewById(R.id.productCard);
            productId = itemView.findViewById(R.id.cartName);
            merchantId = itemView.findViewById(R.id.cartPrice);
            delete = itemView.findViewById(R.id.c_delete);
//            productRating = itemView.findViewById(R.id.textViewRating);
//            productPrice = itemView.findViewById(R.id.textViewPrice);
//            imageView = (ImageView) itemView.findViewById(R.id.imageView);
//            //btDelete = itemView.findViewById(R.id.bt_delete);

        }
    }

    public static interface IAdapterCommunicator {
        void deleteItem(int position);
        //void navigateDetail(String productId);
    }
}



