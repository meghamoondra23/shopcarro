package com.example.meghamoondra.navigationbar.model;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IAccount {

    @POST("signup")
    Call<Boolean> add(@Body NewAccount newaccount);

    @GET("get-product-by-id")
    Call<Product> get(@Query("productId") String productid);

    @GET("get-products")
    Call<Product[]> getAll();

    @GET("search")
    Call<SearchProduct[]> getSearchProducts(@Query("q") String query);

    @POST("get-merchants")
    Call<Stock[]> getMerchantProducts(@Body MerchantModel merchantModel );

    @GET("add-cart")
    Call<Boolean>getCartProducts(@Query("email") String email, @Query("merchantId") String merchantId, @Query("id") String productId );

    @GET("get-history")
    Call<List<History>> getUserHistory(@Query("email") String email);

    @GET("del-item")
    Call<Boolean>getDeleteItem(@Query("email") String email, @Query("merchantId") String merchantId, @Query("id") String productId);

    @GET("get-cart")
    Call<CartProduct>getCart(@Query("email") String email);

    @GET("check-order")
    Call<CartProduct> buyAll(@Query("email") String email);

}
