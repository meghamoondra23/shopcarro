package com.example.meghamoondra.navigationbar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.meghamoondra.navigationbar.model.CartActivity;
import com.example.meghamoondra.navigationbar.model.IAccount;
import com.example.meghamoondra.navigationbar.model.OrderActivity;
import com.example.meghamoondra.navigationbar.model.Product;
import com.example.meghamoondra.navigationbar.model.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.meghamoondra.navigationbar.Urls.addCartUrl;
import static com.example.meghamoondra.navigationbar.Urls.productUrl;

public class ProductDetails extends AppCompatActivity {
    IAccount iAccount;
    Button buyNow;
    Button addToCart;
    TextView productName;
    TextView productDesc;
    TextView productRating;
    TextView productPrice;
    TextView merchantId;
    ImageView imageView;
    String merchantid;
    Product productdetails;
    String productid;
    private static final String ShopcarroPref = "ShopCarroPref";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final SharedPreferences sharedPreferences = getSharedPreferences(ShopcarroPref, Context.MODE_PRIVATE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);


        iAccount = AppController.getInstance().getClientAuthentication(productUrl).create(IAccount.class);

        productid = getIntent().getStringExtra("ProductId");
        merchantid = getIntent().getStringExtra("MerchantId");
        merchantId = findViewById(R.id.pd_id);
        productName = findViewById(R.id.pd_name);
        productDesc = findViewById(R.id.pd_desc);
        productRating = findViewById(R.id.pd_rat);
        productPrice = findViewById(R.id.pd_price);
        imageView = (ImageView) findViewById(R.id.pd_imageView);

        getProductData(productid);

        buyNow = findViewById(R.id.pd_buy);
        addToCart = findViewById(R.id.pd_cart);

        buyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sharedPreferences.contains("username")) {
                    if (sharedPreferences.getString("username", null) != null) {
                        Intent intent = new Intent(ProductDetails.this, CartActivity.class);
                        intent.putExtra("email", sharedPreferences.getString("username", null));
                        intent.putExtra("productId1", productdetails.getId().toString());
                        intent.putExtra("merchantId1", merchantid);
                        intent.putExtra("imageUrl1",productdetails.getImgUrl());
                        intent.putExtra("productName",productdetails.getProductName());
                        startActivity(intent);
                    } else {
                        Intent i = new Intent(ProductDetails.this, LoginActivity.class);
                        startActivity(i);
                    }
                }
            }
        });

        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sharedPreferences.contains("username")) {
                    if (sharedPreferences.getString("username", null) != null) {
                        String email = sharedPreferences.getString("username", null);
                        getAddCart(email, merchantid, productid);
                        Toast.makeText(getApplicationContext(),"Added to the Cart",Toast.LENGTH_LONG).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Go to login",Toast.LENGTH_LONG).show();
                        Intent i = new Intent(ProductDetails.this, LoginActivity.class);
                        startActivity(i);
                    }
                }
                else{
                    Toast.makeText(getApplicationContext(),"Go to login",Toast.LENGTH_LONG).show();
                    Intent i = new Intent(ProductDetails.this, LoginActivity.class);
                    startActivity(i);
                }
            }
        });

    }

    private void getProductData(String productid) {
        Call<Product> call = iAccount.get(productid);
        //Log.d("anything", newAccount.toString());
        call.enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                productdetails = response.body();
                Log.d("123",productdetails.toString());
                merchantId.setText("Merchant Id :"+merchantid);
                productDesc.setText("Description :"+ productdetails.getDescription());
                productPrice.setText("Product Price :" + productdetails.getPrice()+"");
                productName.setText("Product Name :"+ productdetails.getProductName());
                Glide.with(imageView.getContext())
                        .load(productdetails.getImgUrl())
                        .into(imageView);

                Toast.makeText(getApplication(), "Show description page", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                Toast.makeText(getApplication(), "OnFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getAddCart(String email, String merchantId, String productId) {
        IAccount iAccount = AppController.getInstance().getClientAddCart(addCartUrl).create(IAccount.class);
        Call<Boolean> call = iAccount.getCartProducts(email,merchantId,productId);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.code() >= 200 && response.code() < 300) {
                    if(response.body()){
                        Toast.makeText(getApplication(), "Product added to cart", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Intent i = new Intent(ProductDetails.this, LoginActivity.class);
                        startActivity(i);
                    }
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Cart Failure", Toast.LENGTH_SHORT).show();

            }
        });

    }
}
