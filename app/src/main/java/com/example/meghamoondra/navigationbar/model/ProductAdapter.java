package com.example.meghamoondra.navigationbar.model;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.meghamoondra.navigationbar.CardView;
import com.example.meghamoondra.navigationbar.R;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    private static final String TAG = "ProductAdapter";
    private List<Product> mProduct;
    private IAdapterCommunicator iAdapterCommunicator;

    public ProductAdapter(List<Product> products, IAdapterCommunicator iAdapterCommunicator) {
        mProduct = products;
        this.iAdapterCommunicator = iAdapterCommunicator;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Input the layout id
        View view =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.home_list_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final Product product = mProduct.get(position);
        holder.productName.setText(product.getProductName()+" Rs." + String.valueOf(product.getPrice()));
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MerchantModel merchantModel=new MerchantModel();
                List<String> merchantIds = product.getMerchantId();
                String productId = product.getId();
                List<StockId> stockIds = new ArrayList<>();
                for(String merchantId : merchantIds) {
                    stockIds.add(new StockId(merchantId, productId));
                }
                Log.d(TAG, "onClick: " + stockIds);
                merchantModel.setStockIds(stockIds);

                iAdapterCommunicator.navigateDetail(merchantModel,product.getImgUrl());
            }
        });
        if (product.getImgUrl() == null || product.getImgUrl().isEmpty()) {
            holder.imageView.setBackground(ContextCompat.getDrawable(holder.imageView.getContext(), product.getDrawableId()));
        } else {
            Glide.with(holder.imageView.getContext())
                    .load(product.getImgUrl())
                    .into(holder.imageView);
        }
        //Binding UI
    }

    @Override
    public int getItemCount() {
        // Set the list count
        return mProduct.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Declare the views
        LinearLayout cardView;
        TextView productName;
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            cardView =itemView.findViewById(R.id.home_card);
            productName = itemView.findViewById(R.id.book_title);
            imageView = (ImageView) itemView.findViewById(R.id.book_img);
        }
    }

    public static interface IAdapterCommunicator {
        //void deleteItem(int position);
        void navigateDetail(MerchantModel merchantModel,String ImageUrl);
        void deleteItem(int position);


    }
}

