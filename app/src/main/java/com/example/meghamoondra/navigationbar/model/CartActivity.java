package com.example.meghamoondra.navigationbar.model;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.meghamoondra.navigationbar.AppController;
import com.example.meghamoondra.navigationbar.CardView;
import com.example.meghamoondra.navigationbar.LoginActivity;
import com.example.meghamoondra.navigationbar.MainActivity;
import com.example.meghamoondra.navigationbar.ProductDetails;
import com.example.meghamoondra.navigationbar.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.meghamoondra.navigationbar.Urls.addCartUrl;
import static com.example.meghamoondra.navigationbar.Urls.orderUrl;

public class CartActivity extends AppCompatActivity implements CartAdapter.IAdapterCommunicator {

    private static final String TAG = "CartActivity";
    List<CartDetails> productList;
    CartAdapter cartAdapter;
    IAccount iAccount;
    String email;
    String productId;
    String merchantId;
    Button delete;
    Button buy;
    RecyclerView recyclerView;
    private static final String ShopcarroPref = "ShopCarroPref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final SharedPreferences sharedPreferences = getSharedPreferences(ShopcarroPref, Context.MODE_PRIVATE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rv_cart);

        //getting the recyclerview from xml
        recyclerView = (RecyclerView) findViewById(R.id.rv_cart_product);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //initializing the productlist
        // productList = new ArrayList<CartProduct>();

        // String data = getIntent().getExtras().getString("Merchant","defaultKey");

        Button btBuyall = findViewById(R.id.buttonBuyAll);

        if (sharedPreferences.contains("username")) {
            if (sharedPreferences.getString("username", null) != null) {
                email = sharedPreferences.getString("username", null);
                productList = new ArrayList<>();
                getCartProducts(email);
                cartAdapter = new CartAdapter(productList, this);
                Log.d("Sreeraj",productList.toString());
                //setting adapter to recyclerview
                recyclerView.setAdapter(cartAdapter);
            }


        } else {
            Toast.makeText(getApplicationContext(), "Go to login", Toast.LENGTH_LONG).show();
            Intent i = new Intent(CartActivity.this, LoginActivity.class);
            startActivity(i);
        }
        // getDelete(email,productId,merchantId);

        btBuyall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = new ProgressDialog(CartActivity.this);
                progressDialog.show();
                IAccount iAccount = AppController.getInstance().getClientOrder(orderUrl).create(IAccount.class);
                Call<CartProduct> call = iAccount.buyAll(email);

                call.enqueue(new Callback<CartProduct>() {
                    @Override
                    public void onResponse(Call<CartProduct> call, Response<CartProduct> response) {
                        Log.d(TAG, "onResponse: " + response.code());
                        Log.d(TAG, "onResponse: " + call.request().url());
                        if (response.code() >= 200 && response.code() < 300) {
                            // productList.addAll(response.body().getDetails());
                            Log.d("Body", "onResponse: " + response.body());
                            Log.d(TAG, "onResponse: " + productList);
                            Toast.makeText(getApplication(), "Order Purchased Successfully", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            Intent intent;
                            intent = new Intent(CartActivity.this,OrderActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplication(), "Server Problem", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<CartProduct> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Server Problem", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
            }
        });


    }

    private void getCartProducts(String email) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.show();
        IAccount iAccount = AppController.getInstance().getClientAddCart(addCartUrl).create(IAccount.class);
        Call<CartProduct> call = iAccount.getCart(email);
        call.enqueue(new Callback<CartProduct>() {
            @Override
            public void onResponse(Call<CartProduct> call, Response<CartProduct> response) {
                Log.d(TAG, "onResponse: " + response.code());
                Log.d(TAG, "onResponse: " + call.request().url());
                if (response.code() >= 200 && response.code() < 300) {
                    productList.addAll(response.body().getDetails());
                    Log.d("Body", "onResponse: " + response.body());
                    Log.d(TAG, "onResponse: " + productList);
                    Toast.makeText(getApplication(), "Item Deleted", Toast.LENGTH_SHORT).show();
                    cartAdapter.notifyDataSetChanged();
                    progressDialog.dismiss();
                } else {
                    Toast.makeText(getApplication(), "Item Not Deleted", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CartProduct> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Delete Failure", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void getDelete(String email, String merchantId, String productId) {
        IAccount iAccount = AppController.getInstance().getClientAddCart(addCartUrl).create(IAccount.class);
        Call<Boolean> call = iAccount.getDeleteItem(email, merchantId, productId);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body()) {
                        Toast.makeText(getApplication(), "Item Deleted", Toast.LENGTH_SHORT).show();
                        cartAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getApplication(), "Item Not Deleted", Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Delete Failure", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void deleteItem(final int position) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.show();
        IAccount iAccount = AppController.getInstance().getClientAddCart(addCartUrl).create(IAccount.class);
        Call<Boolean> call = iAccount.getDeleteItem(email,productList.get(position).getMerchantId(),productList.get(position).getId());
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                Log.d("delete ", "deleteItem" + response.code());
                if (response.code() >= 200 && response.code() < 300 && response.body() == true) {
                    productList.remove(position);
                    cartAdapter.notifyItemRemoved(position);
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Failed to Delete", Toast.LENGTH_LONG).show();
            }
        });
    }
}

