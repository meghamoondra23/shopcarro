package com.example.meghamoondra.navigationbar;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.meghamoondra.navigationbar.model.IAccount;
import com.example.meghamoondra.navigationbar.model.NewAccount;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.example.meghamoondra.navigationbar.Urls.authenticationUrl;

public class SignUpFragment extends Fragment {
    EditText email, firstName, lastName, phoneNumber, address, password;
    IAccount iAccount;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_signup, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        email=view.findViewById(R.id.et_email);
        firstName=view.findViewById(R.id.et_firstname);
        lastName=view.findViewById(R.id.et_lastname);
        phoneNumber=view.findViewById(R.id.et_number);
        address=view.findViewById(R.id.et_address);
        password=view.findViewById(R.id.et_password);

        AppController appController = AppController.getInstance();
        Retrofit retrofit = appController.getClientAuthentication(authenticationUrl);
        iAccount = retrofit.create(IAccount.class);
       // iAccount = AppController.getInstance().getClientAuthentication().create(IAccount.class);

        view.findViewById(R.id.bt_signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isValidEmail(email.getText()) && isValidPassword(password.getText().toString()) && phoneNumber.length()==10) {

                    NewAccount newaccount = new NewAccount();

                    newaccount.setEmail(email.getText().toString());
                    newaccount.setFirstName(firstName.getText().toString());
                    newaccount.setLastName(lastName.getText().toString());
                    newaccount.setPassword(phoneNumber.getText().toString());
                    newaccount.setPhoneNumber(address.getText().toString());
                    newaccount.setAddress(password.getText().toString());

                    addAccount(newaccount);
                }
                else{
                    Toast.makeText(getActivity(), "Enter valid EmailId or Password", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addAccount(NewAccount newAccount) {
        Call<Boolean> call = iAccount.add(newAccount);
        Log.d("anything", newAccount.toString());
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if(response.body()){
                    //Intent i = new Intent(LogInFragment.this,LogInFragment.class);
                      //  i.putExtra("Employee",1);
                        //  startActivity(i);
                    Toast.makeText(getActivity(), "You are inside login page", Toast.LENGTH_SHORT).show();
                  //  Intent intent = new Intent(SignUpFragment.this, MainActivity.class);
                    //startActivity(intent);
                  //  Toast.makeText(Main2Activity.this, "SUCCESS", Toast.LENGTH_SHORT).show();
                }
                //Toast.makeText(Main2Activity.this, "Failure", Toast.LENGTH_SHORT).show();
                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(getActivity(), "OnFailure", Toast.LENGTH_SHORT).show();
            }
        });

    }







    public boolean isValidEmail(CharSequence email) {
        if (email == null) {
            return false;
        }
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean isValidPassword(String password){

        if(password.length()<8 || password==null){
            return false;
        }
        return true;
    }



}
