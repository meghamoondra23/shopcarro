package com.example.meghamoondra.navigationbar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.meghamoondra.navigationbar.model.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.meghamoondra.navigationbar.Urls.authenticationUrl;

public class LoginActivity extends AppCompatActivity {
    final String TAG = "Login_page";
    private static final String ShopcarroPref = "ShopCarroPref";
    Toolbar toolbar;
    EditText etname;
    EditText ptpassword;
    Button btlogin;
    Button buttonsignup;
    IUser iUser;
    String email;
    String password;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Boolean savelogin;
    CheckBox savelogincheckbox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //toolbar.setTitle("ShopCarro");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.setTitle("ShopCarro");
//        setSupportActionBar(toolbar);

        iUser = AppController.getInstance().getClientAuthentication(authenticationUrl).create(IUser.class);

        etname = (EditText) findViewById(R.id.etName);
        ptpassword = (EditText) findViewById(R.id.etPassword);
        btlogin = (Button) findViewById(R.id.btnLogin);
        email = etname.getText().toString();
        sharedPreferences = getSharedPreferences(ShopcarroPref, Context.MODE_PRIVATE);
        savelogincheckbox = (CheckBox) findViewById(R.id.checkbox);
        editor = sharedPreferences.edit();

        buttonsignup = (Button) findViewById(R.id.buttonSignup);

        btlogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validate()) {
                        User user = new User(email, password);
                        login(user);
                    } else {
                        Toast.makeText(getApplicationContext(), "validation_failed", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            savelogin = sharedPreferences.getBoolean("savelogin", true);
            if (savelogin == true) {
                etname.setText(email);
                ptpassword.setText(password);

            }
        buttonsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Going to Signup", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(LoginActivity.this,SignUpActivity.class);
                startActivity(intent);
            }
        });
        }

        private void login ( final User user){
            Call<Boolean> call = iUser.doLogin(user);
            Log.d("LOGlogin", user.toString());
            call.enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    //Toast.makeText(getActivity(),"",Toast.LENGTH_SHORT).show();
                    if (response.body()) {
                        Log.d("insidelogin","insideLogin");
                        Toast.makeText(getApplicationContext(), "Logged in", Toast.LENGTH_SHORT).show();

                        editor.putBoolean("savelogin", true);
                        editor.putString("username", email);
                        editor.putString("password", password);
                        editor.commit();

                        Toast.makeText(getApplicationContext(),email,Toast.LENGTH_LONG).show();

                        //EditText
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), "Unable to Login", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Server not Responding", Toast.LENGTH_SHORT).show();
                    t.printStackTrace();
                }
            });


        }



        private boolean validate () {

            this.email = etname.getText().toString();
            password = this.ptpassword.getText().toString();
            if (email.isEmpty() || password.isEmpty()) {
                etname.setError("Email field Empty");
                ptpassword.setError("Password field Empty");
                return false;
            }

            return isValidEmail(email) && isValidPassword(password);


        }

        public boolean isValidEmail (CharSequence email){
            if (email == null) {
                Toast.makeText(getApplicationContext(), "Enter valid Email Id", Toast.LENGTH_SHORT).show();
                return false;
            }
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }

        public boolean isValidPassword (String password){
            if (password.length() < 8 || password == null) {
                Toast.makeText(getApplicationContext(), "Password: Enter least 8 characters", Toast.LENGTH_SHORT).show();
                return false;
            }
            return true;
        }
    }

