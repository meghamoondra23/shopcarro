package com.example.meghamoondra.navigationbar.model;

public class CartDetails {

    private String id;
    private String merchantId;

    public CartDetails() {
    }

    public CartDetails(String id, String merchantId) {
        this.id = id;
        this.merchantId = merchantId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    @Override
    public String toString() {
        return "CartDetails{" +
                "id='" + id + '\'' +
                ", merchantId='" + merchantId + '\'' +
                '}';
    }
}
